<div id="views-bootstrap-accordion-<?php print $id ?>" class="<?php print $classes ?>">
  <?php foreach ($rows as $key => $row): ?>
    <div class="panel panel-default">
      <a class="accordion-toggle"
         data-toggle="collapse"
         data-parent="#views-bootstrap-accordion-<?php print $id ?>"
         href="#collapse<?php print $key ?>">
        <div class="panel-heading">
          <h4 class="panel-title">
              <?php print $titles[$key] ?>
          </h4>
        </div>
      </a>

      <div id="collapse<?php print $key ?>" class="panel-collapse collapse">
        <div class="panel-body">
          <?php print $row ?>
        </div>
      </div>
    </div>
  <?php endforeach ?>
</div>
